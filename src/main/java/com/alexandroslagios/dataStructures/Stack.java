package com.alexandroslagios.dataStructures;

import javax.swing.*;
import java.util.Arrays;

public class Stack {
    private int top;
    private static int MAX_ELEMENTS = 1000;
    private int[] arr;

    public Stack() {
        arr = new int[MAX_ELEMENTS];
        top = -1;
    }

    public int getTop() {
        return top;
    }

    /**
     * Checks if the stack is empty.
     * @return boolean - True if the stack is empty.
     */
    public boolean isEmpty() {
        return (top == -1);
    }

    /**
     * Pushes a number to the top of the stack if the stack isn't at full capacity.
     * @param number int - The number we are trying to push.
     * @return boolean - False if the stack is full and we can't push any more
     * numbers
     */
    public boolean push(int number) {
        if (top >= (MAX_ELEMENTS-1)) {
            JOptionPane.showMessageDialog(null, "Stack is full!",
                    "Full stack!", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        else {
            arr[++top] = number;
            return true;
        }
    }

    /**
     * It pops the number at the top of the stack if the stack isn't empty.
     * @return int - The number at the top of the stack or -1 if the stack is empty.
     */
    public int pop() {
        if (isEmpty()) {
            JOptionPane.showMessageDialog(null, "Stack is empty!",
                    "Empty Stack", JOptionPane.ERROR_MESSAGE);
            return -1;
        }
        else {
            int firstOutElement = arr[top--];
            return firstOutElement;
        }
    }

    /**
     * It returns the number at the top of the stack without affecting the stack.
     * @return int - The number at the top of the stack if the stuck isn't empty.
     * If the stack is empty, it returns -1;
     */
    public int peek() {
        if (!isEmpty()) {
            return arr[top];
        }
        else {
            JOptionPane.showMessageDialog(null, "Stack is empty!",
                    "Empty stack!", JOptionPane.ERROR_MESSAGE);
            return -1;
        }
    }

    private int[] getStackArray() {
        int [] stackArray = Arrays.stream(arr).limit(top+1).toArray();
        return stackArray;
    }

    /**
     * Returns the index of the given number in the stack. If the number
     * isn't found in the stack, it returns -1;
     * @param target int - The number we are searching for.
     * @return int - The index of the given number in the stack and -1
     * if the number isn't found in the stack.
     */
    public int findNumberInStack(int target) {
        int index = -1;

        index = Algorithms.findIndexOfNumber(getStackArray(), target);

        return index;
    }

    public int min() {
        if (isEmpty()) {
            return -1;
        }
        int min = Arrays.stream(getStackArray()).min().getAsInt();
        return min;
    }

    public int max() {
        if (isEmpty()) {
            return -1;
        }
        int max = Arrays.stream(getStackArray()).max().getAsInt();
        return max;
    }

    public String printStack() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = top; i >= 0; i--) {
            stringBuilder.append(arr[i] + "  ");
        }

        return stringBuilder.toString();
    }
}
