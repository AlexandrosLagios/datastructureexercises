package com.alexandroslagios.dataStructures;

import java.util.*;
import java.util.stream.Collectors;

public class BST {
    public Node root;
    private Set<Integer> nodeValues;

    public BST() {
        this.root = null;
        this.nodeValues = new TreeSet<>();
    }

    /**
     * Adds a value to the appropriate tree node.
     * @param value int - The number we want to add to the tree.
     */
    public void addNode (int value) {
        Node node = new Node(value);
        nodeValues.add(value);
        if(root == null) {
            root = node;
            return;
        }

        Node current = root;
        Node parent = null;

        while (true) {
            parent = current;
            if (current.value < value) {
                current = current.rightChild;
                if (current == null) {
                    parent.rightChild = node;
                    return;
                }
            }
            else {
                current = current.leftChild;
                if (current == null) {
                    parent.leftChild = node;
                    return;
                }
            }
        }
    }

    /**
     * Deletes a node from the tree and updates the affected nodeValues.
     * @param value int - The value of the node that will be deleted.
     * @return boolean - Returns false if the tree doesn't contain a
     * node with the given value.
     */
    public boolean deleteNode(int value) {
        Node parent = root;
        Node current = root;

        boolean isALeftChild = false;

        while (current.value != value) {
            parent = current;
            if (value < current.value) {
                isALeftChild = true;
                current = current.leftChild;
            }
            else {
                isALeftChild = false;
                current = current.rightChild;
            }

            if (current == null) {
                return false;
            }
        }

        nodeValues.remove(value);

        if (current.leftChild == null && current.rightChild == null) {
            if (current == root) {
                root = null;
            }
            else if (isALeftChild) {
                parent.leftChild = null;
            }
            else {
                parent.rightChild = null;
            }
        }

        else if (current.rightChild == null) {
            if (current == root) {
                root = current.leftChild;
            }
            else if (isALeftChild) {
                parent.leftChild = current.leftChild;
            }
            else {
                parent.rightChild = current.leftChild;
            }
        }

        else if (current.leftChild == null) {
            if (current == root) {
                root = current.rightChild;
            }
            else if (isALeftChild) {
                parent.leftChild = current.rightChild;
            }
            else {
                parent.rightChild = current.rightChild;
            }
        }

        else {
            Node replacementNode = getReplacementNode (current);

            if (current == root) {
                root = replacementNode;
            }
            else if (isALeftChild) {
                parent.leftChild = replacementNode;
            }
            else {
                parent.rightChild = replacementNode;
            }

            replacementNode.leftChild = current.leftChild;
        }

        return true;
    }

    /**
     * Finds the node that should replace the deleted node, starting from the
     * right child of the deleted node, moves up the nodeValues and updates their
     * children in the way.
     * @param deletedNode Node - The node that we want to delete and replace.
     * @return Node - The Node that will replace the deleted node.
     */
    private Node getReplacementNode(Node deletedNode) {
        Node replacementParent = deletedNode;
        Node replacement = deletedNode;
        Node current = replacement.rightChild;

        while (current != null) {
            replacementParent = replacement;
            replacement = current;
            current = current.leftChild;
        }

        if (replacement != deletedNode.rightChild) {
            replacementParent.leftChild = replacement.rightChild;
            replacement.rightChild = deletedNode.rightChild;
        }

        return replacement;
    }

    /**
     * Checks if the binary tree contains a given value.
     * @param value int - The value we are searching for.
     * @return Returns true if it found the value in the tree
     * and false if not.
     */
    public boolean findNode(int value) {
        if (getNode(value) != null) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Returns the position of the value in the TreeSet which contains the values of
     * all the tree's nodes.
     * @param value int - The value we are searching for.
     * @return int - The index of the value if it is contained in the tree and
     * -1 if not.
     */
    public int findNodeInSet(int value) {
        if (nodeValues.contains(value)) {
            int index = nodeValues.stream().collect(Collectors.toList()).indexOf(value);
            return index;
        }
        else {
            return -1;
        }
    }

    /**
     * Finds the node with the minimum value in the tree.
     * @return Node - The leftmost node of the tree.
     */
    public Node findMinNode() {
        Node min = root;
        while (min.leftChild != null) {
            min = min.leftChild;
        }
        return min;
    }

    /**
     * Finds the node with the maximum  value in the tree.
     * @return Node - The rightmost node of the tree.
     */
    public Node findMaxNode() {
        Node max = root;
        while (max.rightChild != null) {
            max = max.rightChild;
        }
        return max;
    }

    /**
     * Gets the node with the given value.
     * @param value int - A value that we are searching for in the tree.
     * @return Node - The node with the given value or null if the given
     * value isn't contained in the tree.
     */
    public Node getNode(int value) {
        Node current = root;
        while (current != null) {
            if (current.value == value) {
                return current;
            }
            else if (current.value < value) {
                current = current.rightChild;
            }
            else {
                current = current.leftChild;
            }
        }
        return null;
    }

    /**
     * Stolen method from GeeksForGeeks that prints a topview of the Tree
     */
    public String printTreeLikeGeeksForGeeksDoIt() {
        class QueueObj {
            Node node;
            int hd;

            QueueObj(Node node, int hd) {
                this.node = node;
                this.hd = hd;
            }
        }

        Queue<QueueObj> q = new LinkedList<QueueObj>();
        Map<Integer, Node> topViewMap = new TreeMap<Integer, Node>();

        if (root == null) {
            return "";
        }
        else {
            q.add(new QueueObj(root, 0));
        }

        while (!q.isEmpty()) {
            QueueObj tmpNode = q.poll();
            if (!topViewMap.containsKey(tmpNode.hd)) {
                topViewMap.put(tmpNode.hd, tmpNode.node);
            }

            if (tmpNode.node.leftChild != null) {
                q.add(new QueueObj(tmpNode.node.leftChild, tmpNode.hd - 1));
            }
            if (tmpNode.node.rightChild != null) {
                q.add(new QueueObj(tmpNode.node.rightChild, tmpNode.hd + 1));
            }

        }
        StringBuilder printedTree = new StringBuilder();
        for (Map.Entry<Integer, Node> entry : topViewMap.entrySet()) {
            printedTree.append(entry.getValue().value + "\n");
        }
        return printedTree.toString();
    }

    /**
     * Creates a String with all the values in the tree in ascending order, which derives
     * naturally from the TreeSet "nodeValues".
     * @return String - All the values in the tree in ascending order, in separate lines.
     */
    public String printTreeFromTreeSet() {
        StringBuilder values = new StringBuilder();
        for (Integer value: nodeValues) {
            values.append(value + "  ");
        }
        return values.toString();
    }
}

class Node {
    int value;

    Node leftChild;
    Node rightChild;

    public Node(int value) {
        this.value = value;
        rightChild = null;
        leftChild = null;
    }
}

