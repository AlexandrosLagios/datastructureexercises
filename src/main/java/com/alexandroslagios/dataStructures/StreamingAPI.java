package com.alexandroslagios.dataStructures;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class StreamingAPI {
    public static void randomNumberStreams() {
        Random random = new Random();
        List<Integer> randomNumberList = random.ints(1000).boxed().collect(Collectors.toList());
        int min = randomNumberList.stream().mapToInt(i -> i).min().getAsInt();
        int max = randomNumberList.stream().mapToInt(i -> i).max().getAsInt();
        long sum = randomNumberList.stream().mapToInt(i -> i).sum();
        double avg = randomNumberList.stream().mapToInt(i -> i).average().getAsDouble();
        System.out.println("The minimum number in the list is " + min);
        System.out.println("The maximum number in the list is " + max);
        System.out.println("The sum of the numbers in the list is " + sum);
        System.out.println("The average of the numbers in the list is " + avg);
        // or:
        IntSummaryStatistics summaryStatistics = randomNumberList.stream().mapToInt(i -> i).summaryStatistics();

        List<Integer> evenNumberList = randomNumberList.stream().filter(i -> i % 2 == 0).collect(Collectors.toList());
        List<Integer> oddNumberList = randomNumberList.stream().filter(i -> i % 2 != 0).collect(Collectors.toList());
    }

    /**
     * Given a list of strings, it returns a list of only the nonempty strings
     * contained in the original list.
     * @param originalStringList List<String> - A list of strings
     * @return List<String> A list of strings without empty strings.
     */
    public static List<String> getNonemptyStrings(List<String> originalStringList) {
        List<String> nonemptyStringList = originalStringList.stream().filter(s -> !s.isEmpty())
                .collect(Collectors.toList());
        return nonemptyStringList;
    }
}
