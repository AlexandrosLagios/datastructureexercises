package com.alexandroslagios.dataStructures;

import com.alexandroslagios.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class BSTMenu extends JFrame implements ActionListener {

    BST tree;
    JButton addNodeButton;
    JButton deleteNodeButton;
    JButton showMinButton;
    JButton showMaxButton;
    JButton findNodeButton;
    JButton printTreeButton;
    JButton mainMenuButton;
    JButton exitButton;


    public BSTMenu(BST tree) {

        this.tree = tree;
        this.setLayout(new GridLayout(8, 1));
        this.setLocationRelativeTo(null);
        this.setTitle("Binary Search Tree Operations Menu");
        List<JButton> buttons = new ArrayList<>();
        addNodeButton = new JButton("Add a node");
        addNodeButton.addActionListener(this::actionPerformed);
        buttons.add(addNodeButton);
        deleteNodeButton = new JButton("Delete a node");
        deleteNodeButton.addActionListener(this::actionPerformed);
        buttons.add(deleteNodeButton);
        showMinButton = new JButton("Show the minimum node");
        showMinButton.addActionListener(this::actionPerformed);
        buttons.add(showMinButton);
        showMaxButton = new JButton("Show the maximum node");
        showMaxButton.addActionListener(this::actionPerformed);
        buttons.add(showMaxButton);
        findNodeButton = new JButton("Find a node");
        findNodeButton.addActionListener(this::actionPerformed);
        buttons.add(findNodeButton);
        printTreeButton = new JButton("Print the tree");
        printTreeButton.addActionListener(this::actionPerformed);
        buttons.add(printTreeButton);
        mainMenuButton = new JButton("Main menu");
        mainMenuButton.addActionListener(this::actionPerformed);
        buttons.add(mainMenuButton);
        exitButton = new JButton("Exit");
        exitButton.addActionListener(this::actionPerformed);
        buttons.add(exitButton);
        for (JButton button : buttons) {
            this.add(button);
        }
        this.setSize(250, 300);
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * The method that determines what will happen after we press each menu button.
     * @param e ActionEvent - the action we performed in the menu.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == addNodeButton)
        {
            int moreNodes = 0;
            // While the user wants to add more nodes, ask for input.
            while (moreNodes != 1)
            {
                boolean textIsInteger = false;

                // Ask for a number until the user inputs an integer.
                while (!textIsInteger) {
                    String text;
                    JLabel label = new JLabel("Write a number to add to the tree:");
                    label.setFont(Main.largeFont);
                    text = JOptionPane.showInputDialog(this, label);
                    textIsInteger = Main.isInteger(text);
                    if(text == null){
                        return;
                    }
                    if (textIsInteger) {
                        tree.addNode(Integer.parseInt(text));
                        label = new JLabel("Do you want to add another node?");
                        label.setFont(Main.largeFont);
                        moreNodes = JOptionPane.showConfirmDialog(this, label,
                                "Add one more node?", JOptionPane.YES_NO_OPTION);
                    }
                    else {
                        label = new JLabel(text + " is not a number!");
                        JOptionPane.showMessageDialog(this, label, "Invalid input!", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        }

        else if (e.getSource() == deleteNodeButton) {
            int moreDeletions = 0;
            // While the user wants to delete more numbers from the tree, ask for input.
            while (moreDeletions != 1)
            {
                JLabel label = new JLabel("Write a number to delete from the tree:");
                label.setFont(Main.largeFont);
                String text = JOptionPane.showInputDialog(label);
                tree.deleteNode(Integer.parseInt(text));

                label = new JLabel("Do you want to delete another node?");
                label.setFont(Main.largeFont);
                moreDeletions = JOptionPane.showConfirmDialog(null, label,
                        "Delete one more node?", JOptionPane.YES_NO_OPTION);
            }
        }

        else if (e.getSource() == findNodeButton) {
            String text = "";

            while (!Main.isInteger(text)) {
                JLabel label = new JLabel("Write a number to find in the tree:");
                label.setFont(Main.largeFont);
                text = JOptionPane.showInputDialog(this, label);
                if(text == null){
                    return;
                }

                if (Main.isInteger(text)){
                    int number = Integer.parseInt(text);
                    int position = tree.findNodeInSet(Integer.parseInt(text)) + 1;
                    if (position != -1) {
                        JLabel foundNumberLabel = new JLabel(number + " was found in the tree, at position "
                                + position);
                        label.setFont(Main.largeFont);
                        JOptionPane.showMessageDialog(this, foundNumberLabel,
                                "Success!", JOptionPane.INFORMATION_MESSAGE);
                    }
                    else {
                        JLabel notFoundNumberLabel = new JLabel(number + " wasn't found in the tree");
                        label.setFont(Main.largeFont);
                        JOptionPane.showMessageDialog(this, notFoundNumberLabel,
                                "Nope!", JOptionPane.ERROR_MESSAGE);
                    }
                }
                else {
                    label = new JLabel(text + " is not a number!");
                    JOptionPane.showMessageDialog(this, label, "Invalid input!", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        else if (e.getSource() == showMinButton) {
            if (tree.root != null) {
                String minNodeMessage = "The minimum node is " + tree.findMinNode().value + ".";
                JOptionPane.showMessageDialog(this, minNodeMessage,
                        "Minimum node", JOptionPane.INFORMATION_MESSAGE);
            }
            else {
                String minNodeMessage = "The tree is empty";
                JOptionPane.showMessageDialog(this, minNodeMessage,
                        "Empty Tree!", JOptionPane.ERROR_MESSAGE);
            }
        }

        else if (e.getSource() == showMaxButton) {
            if (tree.root != null) {
                String maxNodeMessage = "The maximum node is " + tree.findMaxNode().value + ".";
                JOptionPane.showMessageDialog(this, maxNodeMessage,
                        "Maximum node", JOptionPane.INFORMATION_MESSAGE);
            }
            else {
                String minNodeMessage = "The tree is empty";
                JOptionPane.showMessageDialog(this, minNodeMessage,
                        "Empty Tree!", JOptionPane.ERROR_MESSAGE);
            }
        }

        else if (e.getSource() == printTreeButton) {
            String printedTree = tree.printTreeFromTreeSet();
            JOptionPane.showMessageDialog(this, printedTree,
                    "Binary Search Tree Values", JOptionPane.PLAIN_MESSAGE);
        }

        else if (e.getSource() == mainMenuButton) {
            MainMenu mainMenu = new MainMenu();
            this.dispose();
        }


        else if (e.getSource() == exitButton) {
            System.exit(0);
        }
    }
}
