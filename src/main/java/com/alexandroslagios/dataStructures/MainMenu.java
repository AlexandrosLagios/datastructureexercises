package com.alexandroslagios.dataStructures;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class MainMenu extends JFrame implements ActionListener {

    JButton bstButton;
    JButton stackButton;
    JButton exitButton;

    public MainMenu() {
        this.setLayout(new GridLayout(3, 1));
        this.setLocationRelativeTo(null);
        JLabel label = new JLabel();
        this.setTitle("Main Menu");
        bstButton = new JButton("Play with Binary Search Trees");
        this.add(bstButton);
        bstButton.addActionListener(this::actionPerformed);
        stackButton = new JButton("Play with Stacks");
        stackButton.addActionListener(this::actionPerformed);
        this.add(stackButton);
        exitButton = new JButton("Exit");
        exitButton.addActionListener(this::actionPerformed);
        this.add(exitButton);
        this.setSize(250, 250);
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == bstButton) {
            BSTService bstService = new BSTService();
            BSTMenu bstMenu = new BSTMenu(bstService.getTree());
            this.dispose();
        }
        else if (e.getSource() == stackButton) {
            StackService stackService = new StackService();
            StackMenu stackMenu = new StackMenu(stackService.getStack());
            this.dispose();
        }
        else if (e.getSource() == exitButton) {
            System.exit(0);
        }
    }
}
