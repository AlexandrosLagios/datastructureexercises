package com.alexandroslagios.dataStructures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Algorithms {
    /**
     *
     * @param arr
     * @return
     */
    /**
     * Given an array of integers, it finds all the pairs that equal
     * the target sum when summed.
     * @param arr An array of integers.
     * @param targetSum The sum we are searching for in the array.
     * @return void - It prints the pairs it has found with System.out.
     */
    public static void findPairThatMatchesSum(int[] arr, int targetSum) {
        List<int[]> pairs = new ArrayList<int[]>();
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i; j < arr.length; j++) {
                if (arr[i] + arr[j] == targetSum) {
                    pairs.add(new int[]{i, j, arr[i], arr[j]});
                }
            }
        }

        int numberOfPairs = pairs.size();

        String result = "There are no pairs in the array with a sum of "
                + targetSum + ".";

        if (numberOfPairs > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(numberOfPairs + " pairs with a sum of "
                    + targetSum + " were found in the array: \n");
            for (int[] pair : pairs) {
                stringBuilder.append("At indexes " + pair[0] + " and "
                + pair[1] + " (" + pair[2] + "+" + pair[3] + ")\n");
            }
            result = stringBuilder.toString();
        }


        System.out.println(result);
    }

    /**
     * It searches a sorted array for a number linearly (it starts from the first element
     * and iterates through the whole array until it finds the target).
     * @param arr int[]
     * @param target int - The number we are searching for.
     * @return It returns true if the array contains the number and false if it doesn't.
     */
    public static boolean linearSearch(int[] arr, int target) {
        for (int i : arr) {
            if (i == target) {
                return true;
            }
        }

        return false;
    }

    /**
     * It searches a sorted array for a number using the recursive method binarySearchHelper.
     * @param arr int[]
     * @param target int - The number we are searching for.
     * @return It returns true if the array contains the number and false if it doesn't.
     */
    public static boolean binarySearch(int[] arr, int target) {
        return binarySearchHelper(arr, arr[0], arr[arr.length-1], target);
    }

    /**
     * It checks if the element of the array at the index that is at the middle between
     * the left and right parameters is the number we are searching for (target). It it is,
     * the method returns true. If it is smaller than the target, the method calls itself and
     * replaces the right limit with mid-1. If it is larger, the method calls itself and replaces
     * the left limit with mid+1.
     * @param arr int[]
     * @param left int - the left limit (an index of the array)
     * @param right int - the right limit (an index of the array)
     * @param target int - the number we are searching for.
     * @return Returns true if the element at the middle index is the same as the target and
     * false if the left index has become larger than the right one.
     */
    private static boolean binarySearchHelper(int[] arr, int left, int right, int target) {
        if (right < left) {
            return false;
        }

        int mid = (left + right) / 2;

        if (arr[mid] == target) {
            return true;
        }
        else if (target < arr[mid]) {
            return binarySearchHelper(arr, left, mid-1, target);
        }
        else {
            return binarySearchHelper(arr, mid+1, right, target);
        }
    }

    /**
     * It searches an array for a number and returns the first index that it was found
     * (starting from index 0).
     * @param arr int[]
     * @param target int - the number we are searching for.
     * @return The first index from the left of the array where it found the target number.
     * If the array doesn't contain the number, the method returns -1.
     */
    public static int findIndexOfNumberLinearly(int[] arr, int target) {
        for (int i =0; i < arr.length; i++) {
            if (arr[i] == target) {
                return i;
            }
        }

        return -1;
    }

    /**
     * It searches an array for a number with using the recursive method findIndexOfNumberHelper
     * and returns the index where it was found.
     * @param arr int[]
     * @param target The number we are searching for.
     * @return An index of the array where it found the target number.
     * If the array doesn't contain the number, the method returns -1.
     */
    public static int findIndexOfNumber(int[] arr, int target) {
        return findIndexOfNumberHelper(arr, 0, arr.length-1, target);
    }

    private static int findIndexOfNumberHelper(int[] arr, int left, int right, int target) {
        if (right < left) {
            return -1;
        }

        int mid = (left + right) / 2;

        if (target == arr[mid]) {
            return mid;
        }
        else if (target < arr[mid]) {
            return findIndexOfNumberHelper(arr, left, mid-1, target);
        }
        else {
            return findIndexOfNumberHelper(arr, mid+1, right, target);
        }
    }


    /**
     * Counts the number of occurrences of a number inside an array and prints the results
     * with System.out.
     * @param arr int[]
     * @param number The number we are searching for.
     */
    public static void countOccurrencesOfNumber(int[] arr, int number) {
        int startingIndex = findIndexOfNumber(arr, number);
        int counter = 0;
        if (startingIndex != -1) {
            counter = 1;
        }
        else {
            System.out.println("Number " + number + " occurs " + counter + " times in the array.");
        }

        int indexToTheRight = startingIndex+1;
        int indexToTheLeft = startingIndex-1;
        while(indexToTheRight < arr.length && arr[indexToTheRight] == number) {
            counter++;
            indexToTheRight++;
        }
        while (indexToTheLeft >= 0 && arr[indexToTheLeft] == number) {
            counter++;
            indexToTheLeft--;
        }

        System.out.println("Number " + number + " occurs " + counter + " times in the array.");
    }

    /**
     * Counts the number of occurrences of a number inside an array and prints the results
     * with System.out.
     * @param arr int[]
     * @param number The number we are searching for.
     */
    public static void countOfOccurrencesOfNumberWithLinearSearch(int[] arr, int number) {
        int startingIndex = findIndexOfNumberLinearly(arr, number);
        int counter = 0;
        if (startingIndex != -1) {
            counter = 1;
        }
        else {
            System.out.println("Number " + number + " occurs 0 times in the array.");
        }

        int indexToTheRight = startingIndex+1;

        while(indexToTheRight < arr.length && arr[indexToTheRight] == number) {
            counter++;
            indexToTheRight++;
        }

        System.out.println("Number " + number + " occurs " + counter + " times in the array.");
    }
}
