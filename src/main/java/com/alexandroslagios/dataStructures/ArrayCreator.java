package com.alexandroslagios.dataStructures;

import java.util.Arrays;
import java.util.Random;

public class ArrayCreator {
    private int[] array;


    /**
     * An ArrayCreator object is created with an array of a given length
     * and with random numbers between two given borders.
     * @param lengthOfArray int - The length of the created array.
     * @param lowerBorder int - The lower border of the generated random numbers.
     * @param upperBorder int - The
     */
    public ArrayCreator(int lengthOfArray, int lowerBorder, int upperBorder) {
        Random random = new Random();
        this.array = random.ints(lengthOfArray, lowerBorder, upperBorder)
                .toArray();
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    /**
     * Prints the elements of the array of ArrayCreator putting commas between them.
     */
    public void printArray() {
//        System.out.print(array[0]);
//        for (int i = 1; i < array.length; i++) {
//            System.out.print(", " + array[i]);
//        }
//        System.out.println();
        System.out.println(Arrays.toString(array));
    }
}
