package com.alexandroslagios.dataStructures;

import javax.swing.*;

public class StackService {
    private Stack stack;

    public StackService() {
        this.stack = new Stack();
    }

    public Stack getStack() {
        return stack;
    }

    public void setStack(Stack stack) {
        this.stack = stack;
    }

    public void displayMenu() {
        StackMenu menu = new StackMenu(this.stack);
    }


}
