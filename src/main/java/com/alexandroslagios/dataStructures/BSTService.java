package com.alexandroslagios.dataStructures;

public class BSTService {
    private BST tree;

    public BSTService() {
        tree = new BST();
    }

    public BST getTree() {
        return tree;
    }

    public void setTree(BST tree) {
        this.tree = tree;
    }

    public void displayMenu() {
        BSTMenu menu = new BSTMenu(this.tree);
    }

    /**
     * Checks recursively if two trees are identical.
     * @param tree1 BST - The first tree.
     * @param tree2 BST - The second tree.
     * @return boolean - True if all of the nodes are identical in
     * the two trees.
     */
    public boolean treesAreIdentical (BST tree1, BST tree2) {

        boolean treesAreIdentical;

        // If the trees are empty, it returns true.
        if (tree1.root == null && tree1.root == null) {
            return true;
        }

        // If the roots differ, it returns false.
        else if (tree1.root != tree2.root) {
            return false;
        }

        /* if the roots are identical, they are deleted and
        the method calls itself again for the two reduced trees.
         */
        else {
            tree1.deleteNode(tree1.root.value);
            tree2.deleteNode(tree2.root.value);
            treesAreIdentical(tree1, tree2);
        }

        return false;
    }
}
