package com.alexandroslagios.dataStructures;

import com.alexandroslagios.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class StackMenu extends JFrame implements ActionListener {
    Stack stack;
    JButton addToStackButton;
    JButton popFromStackButton;
    JButton showMinMaxButton;
    JButton findNumberButton;
    JButton printStackButton;
    JButton mainMenuButton;
    JButton exitButton;


    public StackMenu(Stack stack) {

        this.stack = stack;
        this.setLayout(new GridLayout(0, 1));
        this.setLocationRelativeTo(null);
        this.setTitle("Stack Operations Menu");
        List<JButton> buttons = new ArrayList<>();
        addToStackButton = new JButton("Push a number to the stack");
        addToStackButton.addActionListener(this::actionPerformed);
        buttons.add(addToStackButton);
        popFromStackButton = new JButton("Pop a number from the stack");
        popFromStackButton.addActionListener(this::actionPerformed);
        buttons.add(popFromStackButton);
        showMinMaxButton = new JButton("Show min and max number");
        showMinMaxButton.addActionListener(this::actionPerformed);
        buttons.add(showMinMaxButton);
        findNumberButton = new JButton("Find a number in the stack");
        findNumberButton.addActionListener(this::actionPerformed);
        buttons.add(findNumberButton);
        printStackButton = new JButton("Print the stack");
        printStackButton.addActionListener(this::actionPerformed);
        buttons.add(printStackButton);
        mainMenuButton = new JButton("Main menu");
        mainMenuButton.addActionListener(this::actionPerformed);
        buttons.add(mainMenuButton);
        exitButton = new JButton("Exit");
        exitButton.addActionListener(this::actionPerformed);
        buttons.add(exitButton);
        for (JButton button : buttons) {
            this.add(button);
        }
        this.setSize(240, 300);
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * The method that determines what will happen after we press each menu button.
     * @param e ActionEvent - the action we performed in the menu.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        JLabel label = new JLabel();
        label.setFont(Main.largeFont);
        if (e.getSource() == addToStackButton)
        {
            int moreNumbers = 0;
            // While the user wants to add more numbers, ask for input.
            while (moreNumbers != 1)
            {
                boolean textIsInteger = false;

                // Ask for a number until the user inputs an integer.
                while (!textIsInteger) {
                    String text;
                    label = new JLabel("Write a number to add to the stack:");
                    text = JOptionPane.showInputDialog(this, label);
                    textIsInteger = Main.isInteger(text);
                    if(text == null){
                        return;
                    }
                    if (textIsInteger) {
                        boolean pushResult = stack.push(Integer.parseInt(text));
                        if (!pushResult) {
                            return;
                        }
                        label = new JLabel("Do you want to push another number?");
                        moreNumbers = JOptionPane.showConfirmDialog(this, label,
                                "Push one more number?", JOptionPane.YES_NO_OPTION);
                    }
                    else {
                        label = new JLabel(text + " is not a number!");
                        JOptionPane.showMessageDialog(this, label, "Invalid input!", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        }

        else if (e.getSource() == popFromStackButton) {
            int moreDeletions = 0;

            // While the user wants to delete more numbers from the tree, ask for input.
            while (moreDeletions != 1)
            {
                if (stack.isEmpty()) {
                    return;
                }

                int popedNumber = stack.pop();

                label = new JLabel("The number at the top of the stack is " + popedNumber +
                        " and it was deleted from the stack.");
                JOptionPane.showMessageDialog(this, label, "Popped number", JOptionPane.PLAIN_MESSAGE);

                label = new JLabel("Do you want to pop another number?");
                moreDeletions = JOptionPane.showConfirmDialog(this, label,
                        "Pop one more number?", JOptionPane.YES_NO_OPTION);
            }
        }

        else if (e.getSource() == findNumberButton) {
            if (stack.isEmpty()) {
                JOptionPane.showMessageDialog(this, "Stack is empty!",
                        "Empty stack!", JOptionPane.ERROR_MESSAGE);
                return;
            }

            String text = "";

            while (!Main.isInteger(text)) {
                label = new JLabel("Write a number to find in the stack:");
                text = JOptionPane.showInputDialog(this, label);

                if(text == null){
                    return;
                }

                if (Main.isInteger(text)){
                    int number = Integer.parseInt(text);
                    int indexInStack = stack.findNumberInStack(Integer.parseInt(text));
                    int position = stack.getTop() + 1 - indexInStack;
                    if (indexInStack != -1) {
                        JLabel foundNumberLabel = new JLabel(number + " was found in the stack, at position "
                                + position + " from the top");
                        JOptionPane.showMessageDialog(this, foundNumberLabel,
                                "Success!", JOptionPane.INFORMATION_MESSAGE);
                    }
                    else {
                        JLabel notFoundNumberLabel = new JLabel(number + " wasn't found in the stack");
                        JOptionPane.showMessageDialog(this, notFoundNumberLabel,
                                "Nope!", JOptionPane.ERROR_MESSAGE);
                    }
                }
                else {
                    label = new JLabel(text + " is not a number!");
                    JOptionPane.showMessageDialog(this, label, "Invalid input!", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        else if (e.getSource() == showMinMaxButton) {
            if (!stack.isEmpty()) {
                String minNodeMessage = "The minimum number in the stack is " + stack.min()
                        + " and the maximum is " + stack.max();
                JOptionPane.showMessageDialog(this, minNodeMessage,
                        "Minimum and Maximum", JOptionPane.INFORMATION_MESSAGE);
            }
            else {
                String minNodeMessage = "The stack is empty";
                JOptionPane.showMessageDialog(this, minNodeMessage,
                        "Empty stack!", JOptionPane.ERROR_MESSAGE);
            }
        }

        else if (e.getSource() == printStackButton) {
            String printedTree = stack.printStack();
            JOptionPane.showMessageDialog(this, printedTree,
                    "Stack values from top to bottom", JOptionPane.PLAIN_MESSAGE);
        }

        else if (e.getSource() == mainMenuButton) {
            MainMenu mainMenu = new MainMenu();
            this.dispose();
        }

        else if (e.getSource() == exitButton) {
            System.exit(0);
        }


    }
}
