package com.alexandroslagios;

import com.alexandroslagios.dataStructures.*;

import java.awt.*;
import java.util.Arrays;

public class Main {

    public static Font largeFont = new Font("Dialog", Font.LAYOUT_LEFT_TO_RIGHT,16);

    public static boolean isInteger(String text) {
        try {
            Integer.parseInt(text);
        }
        catch (NumberFormatException textIsNotAnInteger) {
            System.out.println(text + " is not an integer!");
            return false;
        }
        return true;
    }


    public static void main(String[] args) {
//        ArrayCreator arrayCreator = new ArrayCreator(5000, 0, 50);
//        int[] arr = arrayCreator.getArray();
//        arr = Arrays.stream(arr).sorted().toArray();
//        arrayCreator.setArray(arr);
//        arrayCreator.printArray();
//        Algorithms.findPairThatMatchesSum(arr, 20);
//        System.out.println(Algorithms.linearSearch(arr, 23));
//        System.out.println(Algorithms.binarySearch(arr, 23));
//        Algorithms.countOccurrencesOfNumber(arr, 4);
//        Algorithms.countOfOccurrencesOfNumberWithLinearSearch(arr, 4);
//        StreamingAPI.randomNumberStreams();
        MainMenu mainMenu = new MainMenu();
    }
}
